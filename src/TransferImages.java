
import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Charles Fitzhenry
 */
public class TransferImages {
    
    public static String types = "jpg png gif jpeg mp4 wmv ";

    //Method to return correct directory name based on file date attribute
    public static String getDirectory(String m, String y, String dir) {

        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        int year = Integer.parseInt(y);
        int month = Integer.parseInt(m);

        // Format 09 and 10 etc correctly
        if ((month) < 10) {
            return (dir + "\\" + year + "-0" + month + " (" + months[month - 1] + ")");
        } else {
            return (dir + "\\" + year + "-" + month + " (" + months[month - 1] + ")");
        }

    }

    public static Path createDirectory(String m, String y, String dir) {
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        int year = Integer.parseInt(y);
        int month = Integer.parseInt(m);

        try {
            if ((month) < 10) {
                return Files.createDirectories(Paths.get(dir + "\\" + year + "-0" + month + " (" + months[month - 1] + ")"));
                //Files.delete(Paths.get("E:\\MY PICTURES\\" + year + "-0" + (month + 1) + " (" + months[month] + ")"));
            } else {
                return Files.createDirectories(Paths.get(dir + "\\" + year + "-" + month + " (" + months[month - 1] + ")"));
                //Files.delete(Paths.get("E:\\MY PICTURES\\" + year + "-" + (month + 1) + " (" + months[month] + ")"));
            }

        } catch (Exception e) {
            return null;
        }

    }

    public static boolean checkDirectory(String m, String y, String dir) {
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        int year = Integer.parseInt(y);
        int month = Integer.parseInt(m);

        try {
            if ((month) < 10) {
                if (Files.exists(Paths.get(dir + "\\" + year + "-0" + month + " (" + months[month - 1] + ")"))) {
                    return true;
                } else {
                    return false;
                }

            } else {
                if (Files.exists(Paths.get(dir + "\\" + year + "-" + month + " (" + months[month - 1] + ")"))) {
                    return true;
                } else {
                    return false;
                }

            }
        } catch (Exception e) {
            return false;
        }

    }

    public static int[] copyFiles(File[] files, String dest) {
        int success = 0;
        int exists = 0;
        // loop over files in given directory (this includes folders)
        for (File file : files) {

            if (file.isDirectory()) {
                //If the file is a directory, call the copyFiles method on that directory (This becomes recursive so that all files are processed)
                //System.out.println("Directory: " + file.getName());
                copyFiles(file.listFiles(), dest); // Calls same method again. Recursive action
            } else {
                // If the file is a file, do the copying
                System.out.print("Copying File: " + file.getName() + "... ");
                //Copy File
                SimpleDateFormat sdf = new SimpleDateFormat("MM");
                SimpleDateFormat sdfy = new SimpleDateFormat("yyyy");
                String m = sdf.format(file.lastModified());
                String y = sdfy.format(file.lastModified());

                /*The following block allows the check to only copy file types on a given list. E.g .jpg, .png etc*/
                String extension = "";

                int i = file.getName().lastIndexOf('.');
                if (i >= 0) {
                    extension = file.getName().substring(i + 1);
                }
                
                //This if checks if the file type is allowed
                if (types.contains(extension.toLowerCase())) {
                    if (!checkDirectory(m, y, dest)) {
                        try {
                            Files.copy(file.toPath(), Paths.get(createDirectory(m, y, dest) + "\\" + file.getName()));
                            System.out.println("Copied Successfully!");
                            success++;
                        } catch (Exception e) {
                            if (e.toString().toLowerCase().contains("exist")) {
                                System.out.println("Already Exists!");
                                exists++;
                            }
                        }
                    } else {
                        try {
                            Files.copy(file.toPath(), Paths.get(getDirectory(m, y, dest) + "\\" + file.getName()));
                            System.out.println("Copied Successfully!");
                            success++;
                        } catch (Exception e) {
                            if (e.toString().toLowerCase().contains("exist")) {
                                System.out.println("Already Exists!");
                                exists++;
                            }
                        }
                    }
                }
                else{
                    System.out.println("Not Image File.");
                }
            }
        }
        int[] tmp = {success, exists};
        return tmp;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter Source Folders Directories (and enter done to end):");
        List<String> paths = new LinkedList<String>();
        String sourceDir = s.nextLine();
        while (!"done".equalsIgnoreCase(sourceDir)) {
            paths.add(sourceDir);
            System.out.println("Enter another Source Folder Directory (and enter done to end):");
            sourceDir = s.nextLine();

        }
        System.out.println("Enter Destination Directory (Main Folder):");
        String destDir = s.nextLine();
        int[] t;
        for (String path : paths) {
            File[] files = new File(path).listFiles();
            System.out.println("Now copying from: " + path);
            t = copyFiles(files, destDir);
            System.out.println(t[0] + " File(s) were copied and " + t[1] + " File(s) already existed!");
            System.out.println("Total File(s): " + (t[0] + t[1]));
        }
        System.out.println("All Done!");

    }

}
//E:\Users\Charles\Desktop
//C:\Users\Charles.Charles-PC\Desktop\Desktop